#!/bin/bash

set -ue

readonly PROTO_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

cd "${PROTO_DIR}"
protoc --go_out=. caffe/*.proto
