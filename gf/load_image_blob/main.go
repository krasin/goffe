package main

import (
	"fmt"
	"os"

	"samofly.com/goffe/gf"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Fprintf(os.Stderr, "Usage:\n\tload_image_blob <image>\n")
		os.Exit(1)
	}
	img, err := gf.LoadImage(os.Args[1])
	if err != nil {
		fmt.Fprintf(os.Stderr, "LoadImage(%q): %v\n", os.Args[1], err)
		os.Exit(1)
	}
	fmt.Printf("Loaded image blob of size %v, L1: %f, max val: %f\n", img.Dim, img.L1(), img.MaxValue())
	fmt.Printf("bb[0, |, 100, 100] = %f %f %f\n", img.At(0, 0, 100, 100), img.At(0, 1, 100, 100), img.At(0, 2, 100, 100))
}
