package gf

import (
	"fmt"
	"image"
	"io/ioutil"
	"os"

	_ "image/jpeg"
	_ "image/png"

	"github.com/golang/protobuf/proto"
	"samofly.com/goffe/proto/caffe"
)

// Blob is a multidimensional array of floats.
type Blob struct {
	Dim  []int64
	Data []float32
}

// NewBlob creates a blob of the given size.
func NewBlob(dim ...int64) (*Blob, error) {
	size := int64(1)
	for i, v := range dim {
		if v <= 0 {
			return nil, fmt.Errorf("non-positive dimension #d: %d", i, v)
		}
		size *= v
	}
	newDim := make([]int64, len(dim))
	copy(newDim, dim)
	return &Blob{Dim: newDim, Data: make([]float32, size)}, nil
}

// LoadBinaryBlob reads a blob from a binary protobuf specified by filename.
func LoadBinaryBlob(filename string) (*Blob, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	var bp caffe.BlobProto
	if err := proto.Unmarshal(data, &bp); err != nil {
		return nil, fmt.Errorf("failed to parse BlobProto: %v", err)
	}
	return BlobFromProto(&bp)
}

// BlobFromProto creates a Blob from caffe.BlobProto.
func BlobFromProto(bp *caffe.BlobProto) (*Blob, error) {
	if len(bp.Diff) > 0 || len(bp.DoubleDiff) > 0 {
		return nil, fmt.Errorf("unsupported differential proto")
	}
	var dim []int64
	var size int64
	if bp.Shape != nil {
		size = 1
		for _, v := range bp.GetShape().Dim {
			dim = append(dim, v)
			size *= v
		}
	} else {
		dim = []int64{int64(bp.GetNum()), int64(bp.GetChannels()), int64(bp.GetHeight()), int64(bp.GetWidth())}
		size = dim[0] * dim[1] * dim[2] * dim[3]
	}
	for i, v := range dim {
		if v <= 0 {
			return nil, fmt.Errorf("invalid blob shape at dim #%d: %v", i, dim)
		}
	}
	res := &Blob{Dim: dim, Data: make([]float32, size)}
	if len(bp.Data) > 0 {
		if int64(len(bp.Data)) != size {
			return nil, fmt.Errorf("invalid blob data size: %d, want %d due to dimensions %v", len(bp.Data), size, dim)
		}
		copy(res.Data, bp.Data)
	}

	if len(bp.DoubleData) > 0 {
		if len(bp.Data) > 0 {
			return nil, fmt.Errorf("both data and double_data are set")
		}
		if int64(len(bp.DoubleData)) != size {
			return nil, fmt.Errorf("invalid blob double data size: %d, want %d due to dimensions %v", len(bp.DoubleData), size, dim)
		}
		for i, v := range bp.DoubleData {
			res.Data[i] = float32(v)
		}
	}
	return res, nil
}

func BlobsFromProto(blobs []*caffe.BlobProto) ([]*Blob, error) {
	var res []*Blob
	for i, bp := range blobs {
		bb, err := BlobFromProto(bp)
		if err != nil {
			return nil, fmt.Errorf("blob #%d: %v", i, err)
		}
		res = append(res, bb)
	}
	return res, nil
}

// LoadImage reads an image from a file and returns a blob of
// size 1 x channels x height x width, where channels == 3, and it's BGR.
func LoadImage(filename string) (*Blob, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	img, _, err := image.Decode(f)
	if err != nil {
		return nil, fmt.Errorf("failed to decode image: %v", err)
	}
	w := img.Bounds().Dx()
	h := img.Bounds().Dy()
	wh := w * h
	res := &Blob{Dim: []int64{1, 3, int64(h), int64(w)}, Data: make([]float32, 3*wh)}
	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			// TODO: figure out what happens with semitransparent images
			r, g, b, _ := img.At(x, y).RGBA()
			res.Data[y*w+x] = 255 * float32(b) / 0xFFFF
			res.Data[wh+y*w+x] = 255 * float32(g) / 0xFFFF
			res.Data[2*wh+y*w+x] = 255 * float32(r) / 0xFFFF
		}
	}
	return res, nil
}

// L1 is a L1 norm of the blob, a sum of abs values.
func (bb *Blob) L1() float64 {
	var res float64
	for _, v := range bb.Data {
		if v < 0 {
			v = -v
		}
		res += float64(v)
	}
	return float64(res)
}

// L2 is a sum of squares of values.
func (bb *Blob) L2() float64 {
	var res float64
	for _, v := range bb.Data {
		res += float64(v) * float64(v)
	}
	return res
}

// MaxValue returns the maximum value in the blob.
func (bb *Blob) MaxValue() float32 {
	var res float32
	if len(bb.Data) > 0 {
		res = bb.Data[0]
	}
	for _, v := range bb.Data {
		if v > res {
			res = v
		}
	}
	return res
}

// Idx computes the index in Data for the given coordinates. Will return -1, if out of bounds.
func (bb *Blob) Idx(coords ...int64) int64 {
	var idx int64
	if len(bb.Dim) != len(coords) {
		panic(fmt.Sprintf("At(%q): invalid coordinates dimensions; want exactly %d coordinates", coords, len(bb.Dim)))
	}
	for i, dim := range bb.Dim {
		if coords[i] < 0 || coords[i] >= dim {
			return -1
		}
		idx = idx*dim + coords[i]
	}
	return idx
}

func (bb *Blob) Pos(idx int) []int64 {
	cur := int64(idx)
	res := make([]int64, len(bb.Dim))
	for i := len(bb.Dim) - 1; i >= 0; i-- {
		res[i] = cur % bb.Dim[i]
		cur = cur / bb.Dim[i]
	}
	return res
}

// At returns the value at the specified coordinates. Will return zero, if out of bounds.
func (bb *Blob) At(coords ...int64) float32 {
	idx := bb.Idx(coords...)
	if idx < 0 {
		// Out of bounds.
		return 0
	}
	return bb.Data[idx]
}

// Sub subtracts in place, and returns the pointer to itself.
func (bb *Blob) Sub(other *Blob) *Blob {
	if len(bb.Data) != len(other.Data) {
		panic(fmt.Errorf("Sub: different data lengths, cur: %d, other: %d", len(bb.Data), len(other.Data)))
	}
	for i, v := range other.Data {
		bb.Data[i] -= v
	}
	return bb
}

func (bb *Blob) Mean(axis int) *Blob {
	if axis < 0 || axis >= len(bb.Dim) {
		panic(fmt.Errorf("Mean: invalid axis=%d; number of blob dims: %d", axis, len(bb.Dim)))
	}
	var dim []int64
	var num float32 = 1
	for i, v := range bb.Dim {
		if i == axis {
			dim = append(dim, v)
		} else {
			dim = append(dim, 1)
			num *= float32(v)
		}
	}
	size := bb.Dim[axis]
	res := &Blob{Dim: dim, Data: make([]float32, size)}
	for i, v := range bb.Data {
		p := bb.Pos(i)
		for j := range p {
			if j != axis {
				p[j] = 0
			}
		}
		res.Data[res.Idx(p...)] += v
	}
	for i := range res.Data {
		res.Data[i] /= num
	}
	return res
}
