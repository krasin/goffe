package gf

const METAL_STATIC_CODE = `
#include <metal_stdlib>
using namespace metal;

static void max_pool(texture2d_array<half, access::read> in,
                     texture2d_array<half, access::write> out,
                     uint2 gid,
                     int kernelH, int kernelW, int strideH, int strideW,
                     int padH, int padW) {
  if (gid.x >= out.get_width() || gid.y >= out.get_height()) {
    return;
  }
  const int y = -padH + kernelH/2 + int(gid.y)*strideH;
  const int x = -padW + kernelW/2 + int(gid.x)*strideW;

  for (uint f = 0; f < in.get_array_size(); f++) {
    half v = in.read(uint2(x, y), f)[0];
    for (int yy = y-kernelH/2; yy <= y+kernelH/2; yy++) {
      if (yy < 0 || yy >= int(in.get_height())) {
        continue;
      }
      for (int xx = x-kernelW/2; xx <= x+kernelW/2; xx++) {
        if (xx < 0 || xx >= int(in.get_width())) {
          continue;
        }
        half cur = in.read(uint2(xx, yy), f)[0];
        if (cur > v) {
          v = cur;
        }
      }
    }
    out.write(v, gid, f);
  }
}

static void ave_pool(texture2d_array<half, access::read> in,
                     texture2d_array<half, access::write> out,
                     uint2 gid,
                     int kernelH, int kernelW, int strideH, int strideW,
                     int padH, int padW) {
  if (gid.x >= out.get_width() || gid.y >= out.get_height()) {
    return;
  }
  const int y = -padH + kernelH/2 + int(gid.y)*strideH;
  const int x = -padW + kernelW/2 + int(gid.x)*strideW;

  for (uint f = 0; f < in.get_array_size(); f++) {
    float sum = 0.0;
    for (int yy = y-kernelH/2; yy <= y+kernelH/2; yy++) {
      if (yy < 0 || yy >= int(in.get_height())) {
        continue;
      }
      for (int xx = x-kernelW/2; xx <= x+kernelW/2; xx++) {
        if (xx < 0 || xx >= int(in.get_width())) {
          continue;
        }
        sum += in.read(uint2(xx, yy), f)[0];
      }
    }
    out.write(sum/(kernelH*kernelW), gid, f);
  }
}

static void cross_channel_lrn(texture2d_array<half, access::read> in,
                              texture2d_array<half, access::write> out,
                              uint2 gid,
                              int local_size, float alpha, float beta) {
  if (gid.x >= out.get_width() || gid.y >= out.get_height()) {
    return;
  }

  float sum = 0.0;

  // Compute the first half
  for (int c = 0; c < local_size / 2; c++) {
    float v = in.read(gid, c)[0];
    sum += v*v;
  }
  for (int c = 0; c < int(in.get_array_size()); c++) {
    float tail = 0.0;
    if (c > local_size / 2) {
      tail = in.read(gid, c - local_size/2 - 1)[0];
    }
    float head = 0.0;
    if (c < int(in.get_array_size()) - local_size/2) {
      head = in.read(gid, c + local_size/2)[0];
    }
    sum += head*head - tail*tail;
    float p = 1 + alpha/local_size*sum;
    float q = pow(p, beta);
    out.write(in.read(gid, c)[0] / q, gid, c);
  }
}

`
