package gf

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"math"
	"os"
	"path"
	"reflect"
	"strings"

	"samofly.com/goffe/proto/caffe"
)

func canonicalName(name string) string {
	return strings.Replace(name, "/", "_", -1)
}

func canonicalNames(names []string) []string {
	var res []string
	for _, name := range names {
		res = append(res, canonicalName(name))
	}
	return res
}

// ForwardLayer computes a forward pass for a single layer given the blobs.
func ForwardLayer(layer *caffe.LayerParameter, state map[string]*Blob) (err error) {
	if len(layer.Top) == 0 {
		return errors.New("layer has no tops")
	}
	if len(layer.Bottom) == 0 {
		return errors.New("layer has no bottoms")
	}
	if len(layer.Top) > 1 {
		return fmt.Errorf("layer has more than 1 tops (%d)", len(layer.Top))
	}
	if len(layer.Bottom) > 1 && layer.GetType() != "Concat" {
		return fmt.Errorf("layer has more than 1 bottoms (%d)", len(layer.Bottom))
	}

	var bottoms []*Blob
	for _, v := range layer.Bottom {
		b := state[v]
		if b == nil {
			return fmt.Errorf("bottom blob %s is not found in state", v)
		}
		bottoms = append(bottoms, b)
	}
	bottom := bottoms[0]
	if bottom == nil {
		return fmt.Errorf("bottom blob %s is not found in state", layer.Bottom[0])
	}
	weights, err := BlobsFromProto(layer.Blobs)
	if err != nil {
		return fmt.Errorf("invalid weights: %v", err)
	}
	var top *Blob
	switch layer.GetType() {
	case "Convolution":
		top, err = forwardConvolution(layer.ConvolutionParam, weights, bottom)
	case "ReLU":
		top, err = forwardReLU(layer.ReluParam, bottom)
	case "Pooling":
		top, err = forwardPooling(layer.PoolingParam, bottom)
	case "LRN":
		top, err = forwardLRN(layer.LrnParam, bottom)
	case "Concat":
		top, err = forwardConcat(layer.ConcatParam, bottoms)
	case "Dropout":
		top, err = forwardDropout(bottom)
	case "InnerProduct":
		top, err = forwardInnerProduct(layer.InnerProductParam, weights, bottom)
	case "Softmax":
		top, err = forwardSoftmax(layer.SoftmaxParam, bottom)
	default:
		return fmt.Errorf("not implemented")
	}
	if err != nil {
		return
	}
	state[layer.Top[0]] = top
	fmt.Printf("%s %v: L1: %f, L2: %f\n", layer.Top[0], top.Dim, top.L1(), top.L2())
	if layer.GetName() == "pool1/3x3_s2" {
		fmt.Printf("b[0,0]:\n")
		for y := int64(0); y < 10; y++ {
			for x := int64(0); x < 10; x++ {
				fmt.Printf("%6.2f ", top.At(0, 0, y, x))
			}
			fmt.Println()
		}
	}
	return nil
}

// GenerateLayer generates a forward pass for a single layer given the blobs.
func GenerateLayer(ff io.Writer, swiftParams *swiftGenParameters, layer *caffe.LayerParameter, state map[string]*Blob) (err error) {
	if len(layer.Top) == 0 {
		return errors.New("layer has no tops")
	}
	if len(layer.Bottom) == 0 {
		return errors.New("layer has no bottoms")
	}
	if len(layer.Top) > 1 {
		return fmt.Errorf("layer has more than 1 tops (%d)", len(layer.Top))
	}
	if len(layer.Bottom) > 1 && layer.GetType() != "Concat" {
		return fmt.Errorf("layer has more than 1 bottoms (%d)", len(layer.Bottom))
	}

	var bottoms []*Blob
	for _, v := range layer.Bottom {
		b := state[v]
		if b == nil {
			return fmt.Errorf("bottom blob %s is not found in state", v)
		}
		bottoms = append(bottoms, b)
	}
	bottom := bottoms[0]
	if bottom == nil {
		return fmt.Errorf("bottom blob %s is not found in state", layer.Bottom[0])
	}
	weights, err := BlobsFromProto(layer.Blobs)
	if err != nil {
		return fmt.Errorf("invalid weights: %v", err)
	}
	layerName := canonicalName(layer.GetName())
	var top *Blob
	var shards int = 1
	var weightsName = ""
	var outWeights []float32
	switch layer.GetType() {
	case "Convolution":
		top, err = generateMetalConvolution(ff, layerName, layer.ConvolutionParam, weights, bottom)
		weightsName = layerName
		outWeights = reshuffleMetalConvolutionWeights(layer.ConvolutionParam, weights)
	case "ReLU":
		top, err = forwardReLU(layer.ReluParam, bottom)
		// TODO: when annealing ReLU into the previous layer is automated, remove this hack
		shards = 0
	case "Pooling":
		top, err = generatePooling(ff, layerName, layer.PoolingParam, bottom)
	case "LRN":
		top, err = generateLRN(ff, layerName, layer.LrnParam, bottom)
	case "Concat":
		top, err = forwardConcat(layer.ConcatParam, bottoms)
	case "Dropout":
		top, err = forwardDropout(bottom)
		shards = 0
	case "InnerProduct":
		top, err = generateInnerProduct(ff, layerName, layer.InnerProductParam, weights, bottom)
		weightsName = layerName
		outWeights = weights[0].Data
	case "Softmax":
		top, err = forwardSoftmax(layer.SoftmaxParam, bottom)
	default:
		return fmt.Errorf("not implemented")
	}
	if err != nil {
		return
	}
	state[layer.Top[0]] = top
	fmt.Printf("GenerateLayer: %s %v: L1: %f, L2: %f\n", layer.Top[0], top.Dim, top.L1(), top.L2())
	/*if layer.GetName() == "pool1/3x3_s2" {
		fmt.Printf("b[0,0]:\n")
		for y := int64(0); y < 10; y++ {
			for x := int64(0); x < 10; x++ {
				fmt.Printf("%6.2f ", top.At(0, 0, y, x))
			}
			fmt.Println()
		}
	}*/
	var children []string
	if layer.GetType() == "Concat" {
		children = layer.Bottom
	}
	swiftParams.Blobs = append(swiftParams.Blobs, &BlobParam{Name: canonicalName(layer.Top[0]), Dim: top.Dim, Children: canonicalNames(children)})
	if shards > 0 {
		var offset int
		if len(swiftParams.Layers) > 0 {
			last := swiftParams.Layers[len(swiftParams.Layers)-1]
			offset = last.Offset + len(last.Weights)
		}
		swiftParams.Layers = append(swiftParams.Layers, &LayerParam{Name: canonicalName(layerName),
			WeightsName: weightsName, Weights: outWeights, Offset: offset, Shards: shards,
			Top: canonicalName(layer.Top[0]), Bottoms: canonicalNames(layer.Bottom)})
	}
	return nil
}

// Forward computes a forward pass for the network given the blobs.
func Forward(net *caffe.NetParameter, state map[string]*Blob) error {
	for i, input := range net.Input {
		if state[input] == nil {
			return fmt.Errorf("input blob %s not specified", input)
		}
		dim := net.InputShape[i].Dim
		if !reflect.DeepEqual(dim, state[input].Dim) {
			return fmt.Errorf("input blob %s has unexpected shape. Want: %v, got: %v",
				input, dim, state[input].Dim)
		}
	}
	for _, layer := range net.Layer {
		if err := ForwardLayer(layer, state); err != nil {
			return fmt.Errorf("layer %s, %s: %v", layer.GetName(), layer.GetType(), err)
		}
	}
	return nil
}

type BlobParam struct {
	Name     string
	Parent   string
	Children []string
	Offset   int64
	Dim      []int64
}

type LayerParam struct {
	Name        string
	WeightsName string
	Offset      int // offset in the weights bundle
	Weights     []float32
	Shards      int
	Top         string
	Bottoms     []string
}

type swiftGenParameters struct {
	Blobs  []*BlobParam
	Layers []*LayerParam
}

// Generate generates metal shaders for the network given the blobs.
func Generate(metalName string, swiftName, dataName string, net *caffe.NetParameter, state map[string]*Blob) error {
	// Open a file for the Metal generator output.
	ff, err := os.OpenFile(metalName, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	defer ff.Close()

	sf, err := os.OpenFile(swiftName, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	defer sf.Close()

	df, err := os.OpenFile(dataName, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	defer df.Close()

	fmt.Fprint(ff, METAL_STATIC_CODE)
	fmt.Fprintf(sf, ` // This file is generated. Do not edit unless you have no access to the generator.

import Foundation
import Metal

public class GoogLeNetConfig : NetConfig {
    public init() {}

    public func CreateBlobs(device: MTLDevice) -> [String: MTLTexture] {
        var blobs : [String: MTLTexture] = [:]
`)

	var swiftParams swiftGenParameters
	blobParams := make(map[string]*BlobParam)
	var blobNames []string

	for i, input := range net.Input {
		blobNames = append(blobNames, canonicalName(input))
		blobParams[input] = &BlobParam{Name: canonicalName(input), Dim: net.InputShape[i].Dim}
		if state[input] == nil {
			return fmt.Errorf("input blob %s not specified", input)
		}
		dim := net.InputShape[i].Dim
		if !reflect.DeepEqual(dim, state[input].Dim) {
			return fmt.Errorf("input blob %s has unexpected shape. Want: %v, got: %v",
				input, dim, state[input].Dim)
		}
	}
	for _, layer := range net.Layer {
		if err := GenerateLayer(ff, &swiftParams, layer, state); err != nil {
			return fmt.Errorf("layer %s, %s: %v", layer.GetName(), layer.GetType(), err)
		}
	}

	// Resolve and deduplicate blobs
	for _, blob := range swiftParams.Blobs {
		if prevBlob, ok := blobParams[blob.Name]; ok {
			if !reflect.DeepEqual(blob, prevBlob) {
				return fmt.Errorf("Inconsistent blob size for %q: dim: %v, then dim: %v", blob.Name, prevBlob.Dim, blob.Dim)
			}
			continue
		}
		blobParams[blob.Name] = blob
		blobNames = append(blobNames, blob.Name)
		var i int64
		for _, childName := range blob.Children {
			child := blobParams[childName]
			if child == nil {
				return fmt.Errorf("unknown child blob name %s for blob %s\n", childName, blob.Name)
			}
			if child.Parent != "" {
				return fmt.Errorf("child blob %s already has a parent, blob %s. Can't add it as a child for blob %s", child.Name, child.Parent, blob.Name)
			}
			child.Parent = blob.Name
			child.Offset = i
			i += child.Dim[1]
		}
		if len(blob.Children) > 0 && i != blob.Dim[1] {
			return fmt.Errorf("sum of lengths of child blobs for blob %s is %d, want %d", blob.Name, i, blob.Dim[1])
		}
	}

	genBlobInit := func(blob *BlobParam) {
		fmt.Fprintf(sf, "        blobs[\"%s\"] = initBlob(device, channels: %d, height: %d, width: %d)\n", blob.Name, blob.Dim[1], blob.Dim[2], blob.Dim[3])
	}

	// Generate blobs initializer
	inited := make(map[string]bool)
	for _, blobName := range blobNames {
		blob := blobParams[blobName]
		if inited[blob.Name] {
			continue
		}
		if blob.Parent != "" {
			if !inited[blob.Parent] {
				// We want to initialize sub-blobs after their parents
				genBlobInit(blobParams[blob.Parent])
				inited[blob.Parent] = true
			}
			fmt.Fprintf(sf, "        blobs[\"%s\"] = subBlob(blobs[\"%s\"]!, from: %d, to: %d) // %d\n", blob.Name, blob.Parent, blob.Offset, blob.Offset+blob.Dim[1], blob.Dim[1])
			inited[blob.Name] = true
			continue
		}
		// Regular blob
		genBlobInit(blob)
		inited[blob.Name] = true
	}

	fmt.Fprintf(sf, `        return blobs
    }
`)
	// Weights
	/*for _, layer := range swiftParams.Layers {
		if layer.WeightsName == "" {
			continue
		}
		fmt.Fprintf(sf, "static let %s_weights : [Float] = [\n", layer.WeightsName)
		printSwiftFloatArrayContents(sf, layer.Weights)
		fmt.Fprintf(sf, "        ]\n\n")
	}*/

	fmt.Fprintf(sf, `
    public func CreateWeights(engine: Engine) -> [String: MTLBuffer] {
        let device = engine.metalDevice!
        var res: [String: MTLBuffer] = [:]
        let data = getHalfArrayFromBundle(engine, named: "%s")
        let ptr = UnsafePointer<UInt8>(data)
`, path.Base(dataName))

	for _, layer := range swiftParams.Layers {
		if layer.WeightsName == "" {
			continue
		}
		fmt.Fprintf(sf, "        res[\"%s\"] = device.newBufferWithBytes(ptr.advancedBy(%d), length: %d, options: .StorageModeShared)\n",
			layer.Name, 2*layer.Offset, 2*len(layer.Weights))
		// fmt.Fprintf(sf, "        res[\"%s\"] = initFloatBuf(device, values: %s_weights)\n", layer.Name, layer.WeightsName)
	}

	fmt.Fprintf(sf, `
        return res
    }
`)

	// Layers
	fmt.Fprintf(sf, `
    public func GetLayers() -> [NetLayer] {
        return [
`)
	for _, layer := range swiftParams.Layers {
		if layer.Name == "loss3_classifier" || layer.Name == "prob" {
			fmt.Fprintf(sf, "        //")
		}
		fmt.Fprintf(sf, "            NetLayer(name: \"%s\", weights: \"%s\", shards: %d, top: \"%s\", bottoms: [\"%s\"]),\n",
			layer.Name, layer.WeightsName, layer.Shards, layer.Top, strings.Join(layer.Bottoms, "\", \""))
	}
	fmt.Fprintf(sf, `        ]
    }
}
`)

	// Write weights to the data file.
	// TODO: track offsets.
	for _, layer := range swiftParams.Layers {
		if layer.WeightsName == "" {
			continue
		}
		if err := binary.Write(df, binary.LittleEndian, layer.Weights); err != nil {
			return fmt.Errorf("failed to write floats as bytes: %v", err)
		}
	}

	return nil
}

func forwardDropout(bottom *Blob) (top *Blob, err error) {
	top, err = NewBlob(bottom.Dim...)
	if err != nil {
		return nil, fmt.Errorf("failed to create a top blob: %v", err)
	}
	for i, v := range bottom.Data {
		top.Data[i] = v
	}
	return
}

func forwardReLU(param *caffe.ReLUParameter, bottom *Blob) (top *Blob, err error) {
	if param != nil && param.GetNegativeSlope() != 0 {
		return nil, errors.New("ReLU negative slope is not supported")
	}
	top, err = NewBlob(bottom.Dim...)
	if err != nil {
		return nil, fmt.Errorf("failed to create top blob: %v", err)
	}
	for i, v := range bottom.Data {
		if v > 0 {
			top.Data[i] = v
		}
	}
	return
}

func forwardSoftmax(param *caffe.SoftmaxParameter, bottom *Blob) (top *Blob, err error) {
	if param == nil {
		param = &caffe.SoftmaxParameter{}
	}
	if param.GetAxis() != 1 {
		return nil, fmt.Errorf("Unsupported axis=%d, only 1 is supported", param.GetAxis())
	}
	if bottom.Dim[2] != 1 || bottom.Dim[3] != 1 {
		return nil, fmt.Errorf("Unsupported bottom dims: %v, want {n, c, 1, 1}", bottom.Dim)
	}
	top, err = NewBlob(bottom.Dim...)
	if err != nil {
		return nil, fmt.Errorf("failed to create top blob: %v", err)
	}
	for n := int64(0); n < bottom.Dim[0]; n++ {
		tmp := make([]float32, bottom.Dim[1])
		max := bottom.At(n, 0, 0, 0)
		for i := range tmp {
			v := bottom.At(n, int64(i), 0, 0)
			if v > max {
				max = v
			}
			tmp[i] = v
		}
		var sum float32
		for i := range tmp {
			tmp[i] = float32(math.Exp(float64(tmp[i] - max)))
			sum += tmp[i]
		}
		for i := range tmp {
			top.Data[top.Idx(n, int64(i), 0, 0)] = tmp[i] / sum
		}
	}
	return
}

func forwardInnerProduct(param *caffe.InnerProductParameter, weights []*Blob, bottom *Blob) (top *Blob, err error) {
	if param == nil {
		param = &caffe.InnerProductParameter{}
	}
	if param.GetAxis() != 1 {
		return nil, fmt.Errorf("unsupported axis=%d, only 1 is supported", param.GetAxis())
	}
	numOutput := int64(param.GetNumOutput())
	top, err = NewBlob(bottom.Dim[0], numOutput, 1, 1)
	if err != nil {
		return nil, fmt.Errorf("failed to create a top blob: %v", err)
	}
	for n := int64(0); n < bottom.Dim[0]; n++ {
		for f := int64(0); f < numOutput; f++ {
			var sum float32
			for c := int64(0); c < bottom.Dim[1]; c++ {
				for y := int64(0); y < bottom.Dim[2]; y++ {
					for x := int64(0); x < bottom.Dim[3]; x++ {
						sum += weights[0].At(0, 0, f, c) * bottom.At(n, c, y, x)
					}
				}
			}
			top.Data[top.Idx(n, f, 0, 0)] = sum + weights[1].At(0, 0, 0, f)
		}
	}
	return
}

func forwardConcat(param *caffe.ConcatParameter, bottoms []*Blob) (top *Blob, err error) {
	if len(bottoms) == 0 {
		return nil, errors.New("no bottom blobs")
	}
	if param == nil {
		param = &caffe.ConcatParameter{}
	}
	if param.GetAxis() != 1 {
		return nil, fmt.Errorf("axis=%d not supported. Only 1 is supported", param.GetAxis())
	}
	n := bottoms[0].Dim[0]
	c := int64(0)
	h := bottoms[0].Dim[2]
	w := bottoms[0].Dim[3]
	for i, b := range bottoms {
		if n != b.Dim[0] || h != b.Dim[2] || w != b.Dim[3] {
			return nil, fmt.Errorf("bottom #%d has invalid shape %v, want: {%d, ?, %d, %d}", i, b.Dim, n, h, w)
		}
		c += b.Dim[1]
	}
	top, err = NewBlob(n, c, h, w)
	if err != nil {
		return nil, fmt.Errorf("failed to create a top blob: %v", err)
	}
	// TODO: implement copy blobs and slices
	for i := int64(0); i < n; i++ {
		sum := int64(0)
		for _, b := range bottoms {
			for ch := int64(0); ch < b.Dim[1]; ch++ {
				for y := int64(0); y < h; y++ {
					for x := int64(0); x < w; x++ {
						top.Data[top.Idx(i, sum, y, x)] = b.At(i, ch, y, x)
					}
				}
				sum++
			}
		}
	}
	return
}

func forwardLRN(param *caffe.LRNParameter, bottom *Blob) (top *Blob, err error) {
	if param == nil {
		return nil, errors.New("lrn_param is not set")
	}
	if param.GetNormRegion() != caffe.LRNParameter_ACROSS_CHANNELS {
		return nil, fmt.Errorf("unsupported norm_region: %v", param.GetNormRegion())
	}
	k := param.GetK()
	if math.Abs(float64(k)-1) > 1E-4 {
		return nil, fmt.Errorf("unsupported k=%f, only 1 is supported", k)
	}
	size := param.GetLocalSize()
	if size%2 == 0 {
		return nil, fmt.Errorf("unsupported local_size=%d, only odd values are supported")
	}
	alpha := param.GetAlpha()
	beta := param.GetBeta()
	top, err = NewBlob(bottom.Dim...)
	if err != nil {
		return nil, fmt.Errorf("failed to create a top blob: %v", err)
	}
	// Compute across-channel LRN
	for n := int64(0); n < bottom.Dim[0]; n++ {
		for y := int64(0); y < bottom.Dim[2]; y++ {
			for x := int64(0); x < bottom.Dim[3]; x++ {
				var sum float64 = 0
				// Compute the first half
				for c := int64(0); c < int64(size/2); c++ {
					v := float64(bottom.At(n, c, y, x))
					sum += v * v
				}
				for c := int64(0); c < bottom.Dim[1]; c++ {
					tail := float64(bottom.At(n, c-int64(size/2)-1, y, x))
					head := float64(bottom.At(n, c+int64(size/2), y, x))
					sum += head*head - tail*tail
					// Double-check (slower)
					/*var realSum float64
					for i := c - int64(size/2); i <= c+int64(size/2); i++ {
						v := float64(bottom.At(n, i, y, x))
						realSum += v * v
					}
					if math.Abs(sum-realSum) > 1E-3 {
						return nil, fmt.Errorf("n=%d, c=%d, y=%d, x=%d, sum=%f != realSum=%f",
							n, c, y, x, sum, realSum)
					}*/
					p := 1 + alpha/float32(size)*float32(sum)
					if math.IsNaN(float64(p)) {
						return nil, fmt.Errorf("n=%d, c=%d, y=%d, x=%d, p is NaN", n, c, y, x)
					}
					q := math.Pow(float64(p), float64(beta))
					if math.IsNaN(q) {
						return nil, fmt.Errorf("n=%d, c=%d, y=%d, x=%d, p=%f, beta=%f, q is NaN",
							n, c, y, x, p, beta)
					}
					top.Data[top.Idx(n, c, y, x)] = bottom.At(n, c, y, x) / float32(q)

				}
			}
		}
	}
	return
}

func forwardPooling(param *caffe.PoolingParameter, bottom *Blob) (top *Blob, err error) {
	if param == nil {
		return nil, errors.New("pooling param is not set")
	}
	if param.GetPool() != caffe.PoolingParameter_MAX &&
		param.GetPool() != caffe.PoolingParameter_AVE {
		return nil, fmt.Errorf("pooling type %v not supported", param.GetPool())
	}
	if param.GetGlobalPooling() {
		return nil, errors.New("global pooling not supported")
	}
	var padH, padW, kernelH, kernelW, strideH, strideW int64
	if param.GetPad() != 0 {
		padH = int64(param.GetPad())
		padW = int64(param.GetPad())
	} else {
		padH = int64(param.GetPadH())
		padW = int64(param.GetPadW())
	}
	if param.GetKernelSize() != 0 {
		kernelH = int64(param.GetKernelSize())
		kernelW = int64(param.GetKernelSize())
	} else {
		kernelH = int64(param.GetKernelH())
		kernelW = int64(param.GetKernelW())
	}
	if param.GetStride() != 0 {
		strideH = int64(param.GetStride())
		strideW = int64(param.GetStride())
	} else {
		strideH = int64(param.GetStrideH())
		strideW = int64(param.GetStrideW())
	}
	if strideH == 0 {
		strideH = 1
	}
	if strideW == 0 {
		strideW = 1
	}
	fmt.Printf("padH: %d, kernelH: %d, strideH: %d, padW: %d, kernelW: %d, strideW: %d\n",
		padH, kernelH, strideH, padW, kernelW, strideW)
	// We round it up, see https://github.com/BVLC/caffe/issues/1318
	height := (bottom.Dim[2]+2*padH-kernelH+(strideH-1))/strideH + 1
	width := (bottom.Dim[3]+2*padW-kernelW+(strideW-1))/strideW + 1

	res, err := NewBlob(bottom.Dim[0], bottom.Dim[1], height, width)
	if err != nil {
		return nil, fmt.Errorf("could not create top blob: %v", err)
	}

	for n := int64(0); n < bottom.Dim[0]; n++ {
		for f := int64(0); f < bottom.Dim[1]; f++ {
			for j := int64(0); j < height; j++ {
				y := -padH + kernelH/2 + j*strideH
				for i := int64(0); i < width; i++ {
					x := -padW + kernelW/2 + i*strideW
					var agg float32
					if param.GetPool() == caffe.PoolingParameter_MAX {
						agg = bottom.At(n, f, y, x)
					}
					for fy := int64(0); fy < kernelH; fy++ {
						for fx := int64(0); fx < kernelW; fx++ {
							v := bottom.At(n, f, y-kernelH/2+fy, x-kernelW/2+fx)
							switch param.GetPool() {
							case caffe.PoolingParameter_MAX:
								if v > agg {
									agg = v
								}
							case caffe.PoolingParameter_AVE:
								agg += v / (float32(kernelH) * float32(kernelW))
							default:
								panic("unreachable")
							}
						}
					}
					res.Data[res.Idx(n, f, j, i)] = agg
				}
			}
		}
	}
	return res, nil
}

func forwardConvolution(param *caffe.ConvolutionParameter, weights []*Blob, bottom *Blob) (top *Blob, err error) {
	if param == nil {
		return nil, errors.New("convolution param is not set")
	}
	if bottom == nil {
		return nil, errors.New("bottom blob is not set")
	}
	if len(bottom.Dim) != 4 {
		return nil, fmt.Errorf("Unsupported bottom blob dimension: %d, only 4 is supported", len(bottom.Dim))
	}
	numOutput := int64(param.GetNumOutput())
	var padH, padW, kernelH, kernelW, strideH, strideW int64
	if param.GetPad() != 0 {
		padH = int64(param.GetPad())
		padW = int64(param.GetPad())
	} else {
		padH = int64(param.GetPadH())
		padW = int64(param.GetPadW())
	}
	if param.GetKernelSize() != 0 {
		kernelH = int64(param.GetKernelSize())
		kernelW = int64(param.GetKernelSize())
	} else {
		kernelH = int64(param.GetKernelH())
		kernelW = int64(param.GetKernelW())
	}
	if param.GetStride() != 0 {
		strideH = int64(param.GetStride())
		strideW = int64(param.GetStride())
	} else {
		strideH = int64(param.GetStrideH())
		strideW = int64(param.GetStrideW())
	}
	if strideH == 0 {
		strideH = 1
	}
	if strideW == 0 {
		strideW = 1
	}
	fmt.Printf("LALA: %d %dx%d (kern %d, stride %d)\n", numOutput*bottom.Dim[1], bottom.Dim[2], bottom.Dim[3], kernelH, strideH)
	biasTerm := param.GetBiasTerm()
	group := param.GetGroup()
	if group != 1 {
		return nil, fmt.Errorf("grouping not supported, but group=%d", group)
	}
	fmt.Printf("numOutput: %d, padH: %d, kernelH: %d, strideH: %d, padW: %d,"+
		"kernelW: %d, strideW: %d, biasTerm: %v, group: %v\n",
		numOutput, padH, kernelH, strideH, padW, kernelW, strideW, biasTerm, group)
	height := (bottom.Dim[2]+2*padH-kernelH)/strideH + 1
	width := (bottom.Dim[3]+2*padW-kernelW)/strideW + 1

	res, err := NewBlob(bottom.Dim[0], numOutput, height, width)
	if err != nil {
		return nil, fmt.Errorf("could not create top blob: %v", err)
	}

	for n := int64(0); n < bottom.Dim[0]; n++ {
		for f := int64(0); f < numOutput; f++ {
			for j := int64(0); j < height; j++ {
				y := -padH + kernelH/2 + j*strideH
				for i := int64(0); i < width; i++ {
					x := -padW + kernelW/2 + i*strideW
					var sum float32
					// Now, we need to apply the filter #f at (x, y) at the bottom blob.
					for fc := int64(0); fc < bottom.Dim[1]; fc++ {
						for fy := int64(0); fy < kernelH; fy++ {
							for fx := int64(0); fx < kernelW; fx++ {
								weight := weights[0].At(f, fc, fy, fx)
								sum += weight * bottom.At(n, fc, y-kernelH/2+fy, x-kernelW/2+fx)
							}
						}
					}
					// Add bias, if needed.
					if biasTerm {
						sum += weights[1].At(0, 0, 0, f)
					}
					res.Data[res.Idx(n, f, j, i)] = sum
				}
			}
		}
	}
	return res, nil
}

func generateInnerProduct(ff io.Writer, layerName string, param *caffe.InnerProductParameter, weights []*Blob, bottom *Blob) (top *Blob, err error) {
	if param == nil {
		param = &caffe.InnerProductParameter{}
	}
	if param.GetAxis() != 1 {
		return nil, fmt.Errorf("unsupported axis=%d, only 1 is supported", param.GetAxis())
	}
	numOutput := int64(param.GetNumOutput())
	top, err = NewBlob(bottom.Dim[0], numOutput, 1, 1)
	if err != nil {
		return nil, fmt.Errorf("failed to create a top blob: %v", err)
	}
	for n := int64(0); n < bottom.Dim[0]; n++ {
		for f := int64(0); f < numOutput; f++ {
			var sum float32
			for c := int64(0); c < bottom.Dim[1]; c++ {
				for y := int64(0); y < bottom.Dim[2]; y++ {
					for x := int64(0); x < bottom.Dim[3]; x++ {
						sum += weights[0].At(0, 0, f, c) * bottom.At(n, c, y, x)
					}
				}
			}
			top.Data[top.Idx(n, f, 0, 0)] = sum + weights[1].At(0, 0, 0, f)
		}
	}
	return
}

func generateLRN(ff io.Writer, layerName string, param *caffe.LRNParameter, bottom *Blob) (top *Blob, err error) {
	if param == nil {
		return nil, errors.New("lrn_param is not set")
	}
	if param.GetNormRegion() != caffe.LRNParameter_ACROSS_CHANNELS {
		return nil, fmt.Errorf("unsupported norm_region: %v", param.GetNormRegion())
	}
	k := param.GetK()
	if math.Abs(float64(k)-1) > 1E-4 {
		return nil, fmt.Errorf("unsupported k=%f, only 1 is supported", k)
	}
	size := param.GetLocalSize()
	if size%2 == 0 {
		return nil, fmt.Errorf("unsupported local_size=%d, only odd values are supported")
	}
	alpha := param.GetAlpha()
	beta := param.GetBeta()

	fmt.Fprintf(ff, `kernel void %s_0(texture2d_array<half, access::read> in [[texture(0)]],
                         texture2d_array<half, access::write> out [[texture(1)]],
                         uint2 gid [[thread_position_in_grid]]) {
`, layerName)
	fmt.Fprintf(ff, `    const int local_size = %d;
    const float alpha = %f;
    const float beta = %f;
`, size, alpha, beta)

	fmt.Fprintf(ff, "    cross_channel_lrn(in, out, gid, local_size, alpha, beta);\n")
	fmt.Fprintf(ff, "}\n\n")

	return NewBlob(bottom.Dim...)
}

func generatePooling(ff io.Writer, layerName string, param *caffe.PoolingParameter, bottom *Blob) (top *Blob, err error) {
	if param == nil {
		return nil, errors.New("pooling param is not set")
	}
	if param.GetPool() != caffe.PoolingParameter_MAX &&
		param.GetPool() != caffe.PoolingParameter_AVE {
		return nil, fmt.Errorf("pooling type %v not supported", param.GetPool())
	}
	if param.GetGlobalPooling() {
		return nil, errors.New("global pooling not supported")
	}
	var padH, padW, kernelH, kernelW, strideH, strideW int64
	if param.GetPad() != 0 {
		padH = int64(param.GetPad())
		padW = int64(param.GetPad())
	} else {
		padH = int64(param.GetPadH())
		padW = int64(param.GetPadW())
	}
	if param.GetKernelSize() != 0 {
		kernelH = int64(param.GetKernelSize())
		kernelW = int64(param.GetKernelSize())
	} else {
		kernelH = int64(param.GetKernelH())
		kernelW = int64(param.GetKernelW())
	}
	if param.GetStride() != 0 {
		strideH = int64(param.GetStride())
		strideW = int64(param.GetStride())
	} else {
		strideH = int64(param.GetStrideH())
		strideW = int64(param.GetStrideW())
	}
	if strideH == 0 {
		strideH = 1
	}
	if strideW == 0 {
		strideW = 1
	}
	fmt.Printf("padH: %d, kernelH: %d, strideH: %d, padW: %d, kernelW: %d, strideW: %d\n",
		padH, kernelH, strideH, padW, kernelW, strideW)
	// We round it up, see https://github.com/BVLC/caffe/issues/1318
	height := (bottom.Dim[2]+2*padH-kernelH+(strideH-1))/strideH + 1
	width := (bottom.Dim[3]+2*padW-kernelW+(strideW-1))/strideW + 1

	fmt.Fprintf(ff, `kernel void %s_0(texture2d_array<half, access::read> in [[texture(0)]],
                         texture2d_array<half, access::write> out [[texture(1)]],
                         uint2 gid [[thread_position_in_grid]]) {
`, layerName)
	fmt.Fprintf(ff, `    const int kernelH = %d;
    const int kernelW = %d;
    const int strideH = %d;
    const int strideW = %d;
    const int padH = %d;
    const int padW = %d;
`, kernelH, kernelW, strideH, strideW, padH, padW)

	switch param.GetPool() {
	case caffe.PoolingParameter_MAX:
		fmt.Fprintf(ff, "    max_pool(in, out, gid, kernelH, kernelW, strideH, strideW, padH, padW);\n")
	case caffe.PoolingParameter_AVE:
		fmt.Fprintf(ff, "    ave_pool(in, out, gid, kernelH, kernelW, strideH, strideW, padH, padW);\n")
	default:
		return nil, fmt.Errorf("pooling type = %v is not supported", param.GetPool())
	}
	fmt.Fprintf(ff, "}\n\n")

	return NewBlob(bottom.Dim[0], bottom.Dim[1], height, width)
}

func reshuffleMetalConvolutionWeights(param *caffe.ConvolutionParameter, weights []*Blob) []float32 {
	// Caffe order != metal order.
	dim := weights[0].Dim
	numOutput := dim[0]
	numChannels := dim[1]
	height := dim[2]
	width := dim[3]
	res, err := NewBlob(numChannels, height, width, numOutput)
	if err != nil {
		panic(fmt.Errorf("NewBlob: %v", err))
	}
	for fc := int64(0); fc < numChannels; fc++ {
		for y := int64(0); y < height; y++ {
			for x := int64(0); x < width; x++ {
				for f := int64(0); f < numOutput; f++ {
					res.Data[res.Idx(fc, y, x, f)] = weights[0].At(f, fc, y, x)
				}
			}
		}
	}
	return res.Data
}

func generateMetalConvolution(ff io.Writer, layerName string, param *caffe.ConvolutionParameter, weights []*Blob, bottom *Blob) (top *Blob, err error) {
	numOutput := int64(param.GetNumOutput())
	var padH, padW, kernelH, kernelW, strideH, strideW int64
	if param.GetPad() != 0 {
		padH = int64(param.GetPad())
		padW = int64(param.GetPad())
	} else {
		padH = int64(param.GetPadH())
		padW = int64(param.GetPadW())
	}
	if param.GetKernelSize() != 0 {
		kernelH = int64(param.GetKernelSize())
		kernelW = int64(param.GetKernelSize())
	} else {
		kernelH = int64(param.GetKernelH())
		kernelW = int64(param.GetKernelW())
	}
	if param.GetStride() != 0 {
		strideH = int64(param.GetStride())
		strideW = int64(param.GetStride())
	} else {
		strideH = int64(param.GetStrideH())
		strideW = int64(param.GetStrideW())
	}
	if strideH == 0 {
		strideH = 1
	}
	if strideW == 0 {
		strideW = 1
	}
	biasTerm := param.GetBiasTerm()
	group := param.GetGroup()
	if group != 1 {
		return nil, fmt.Errorf("grouping not supported, but group=%d", group)
	}
	fmt.Printf("numOutput: %d, padH: %d, kernelH: %d, strideH: %d, padW: %d,"+
		"kernelW: %d, strideW: %d, biasTerm: %v, group: %v\n",
		numOutput, padH, kernelH, strideH, padW, kernelW, strideW, biasTerm, group)
	height := (bottom.Dim[2]+2*padH-kernelH)/strideH + 1
	width := (bottom.Dim[3]+2*padW-kernelW)/strideW + 1

	// Generate bias array
	if biasTerm {
		printHalfArray(ff, fmt.Sprintf("%s_bias", layerName), weights[1].Data)
	}

	fmt.Fprintf(ff, `kernel void %s_0(texture2d_array<half, access::read> in [[texture(0)]],
                         texture2d_array<half, access::write> out [[texture(1)]],
                         device half* weights [[buffer(0)]],
                         uint2 gid [[thread_position_in_grid]]) {
`, layerName)
	// Metal runs as 16x16 thread blocks. Some of the threads will inevitably fall out of bounds, if width and height are not multiples of 16.
	if width%16 != 0 || height%16 != 0 {
		fmt.Fprintf(ff, "    if (gid.x >= out.get_width() || gid.y >= out.get_height()) { return; }\n")
	}
	var biasStr = ""
	// Add bias, if needed.
	if biasTerm {
		biasStr = fmt.Sprintf("%s_bias[f]", layerName)
	}

	fmt.Fprintf(ff, "    const int kernelH = %d;\n", kernelH)
	fmt.Fprintf(ff, "    const int kernelW = %d;\n", kernelW)
	fmt.Fprintf(ff, "    const int strideH = %d;\n", strideH)
	fmt.Fprintf(ff, "    const int strideW = %d;\n", strideW)
	fmt.Fprintf(ff, "    const int padH = %d;\n", padH)
	fmt.Fprintf(ff, "    const int padW = %d;\n", padW)
	fmt.Fprintf(ff, "    const int y = -padH + kernelH/2 + int(gid.y)*strideH;\n")
	fmt.Fprintf(ff, "    const int x = -padW + kernelW/2 + int(gid.x)*strideW;\n")
	fmt.Fprintf(ff, "    float sum[%d];\n", numOutput)
	fmt.Fprintf(ff, "    half hsum[%d];\n", numOutput)
	fmt.Fprintf(ff, "    for (int f = 0; f < %d; f++) {\n", numOutput)
	fmt.Fprintf(ff, "        sum[f] = %s;\n", biasStr)
	fmt.Fprintf(ff, "        hsum[f] = 0;\n")
	fmt.Fprintf(ff, "    }\n")
	fmt.Fprintf(ff, "    int i = 0;\n")
	fmt.Fprintf(ff, "    for (int fc = 0; fc < %d; fc++) {\n", bottom.Dim[1])
	fmt.Fprintf(ff, "        for (int dy = -kernelH/2; dy <= kernelH/2; dy++) {\n")
	fmt.Fprintf(ff, "            for (int dx = -kernelW/2; dx <= kernelW/2; dx++) {\n")
	fmt.Fprintf(ff, "                if (y+dy >= 0 && y+dy < %d && x+dx >= 0 && x+dx < %d) {\n", bottom.Dim[2], bottom.Dim[3])
	fmt.Fprintf(ff, "                    half v = in.read(uint2(x + dx, y + dy), fc)[0];\n")
	fmt.Fprintf(ff, "                    for (int f = 0; f < %d; f++) {\n", numOutput)
	fmt.Fprintf(ff, "                        half w = weights[i];\n")
	fmt.Fprintf(ff, "                        hsum[f] += w * v;\n")
	fmt.Fprintf(ff, "                        i++;\n")
	fmt.Fprintf(ff, "                    }\n")
	fmt.Fprintf(ff, "                    if ((i / %d) %% 8 == 0) {\n", numOutput)
	fmt.Fprintf(ff, "                        for (int f = 0; f < %d; f++) {\n", numOutput)
	fmt.Fprintf(ff, "                            sum[f] += hsum[f];\n")
	fmt.Fprintf(ff, "                            hsum[f] = 0;\n")
	fmt.Fprintf(ff, "                        }\n")
	fmt.Fprintf(ff, "                    }\n")
	fmt.Fprintf(ff, "                    continue;\n")
	fmt.Fprintf(ff, "                }\n")
	fmt.Fprintf(ff, "                i += %d;\n", numOutput)
	fmt.Fprintf(ff, "            }\n")
	fmt.Fprintf(ff, "        }\n")
	fmt.Fprintf(ff, "    }\n")
	fmt.Fprintf(ff, "    for (int f = 0; f < %d; f++) {\n", numOutput)
	fmt.Fprintf(ff, "        sum[f] += hsum[f];\n")
	fmt.Fprintf(ff, "        hsum[f] = 0;\n")
	fmt.Fprintf(ff, "    }\n")

	// TODO: conditionally do ReLU, only if needed.
	fmt.Fprintln(ff)
	fmt.Fprintf(ff, "    for (int f = 0; f < %d; f++) {\n", numOutput)
	fmt.Fprintf(ff, "        // Pair with a ReLU layer that goes next.\n")
	fmt.Fprintf(ff, "        half v = max(sum[f], 0.0f);\n")
	fmt.Fprintf(ff, "        out.write(v, gid, f);\n")
	fmt.Fprintf(ff, "    }\n")
	fmt.Fprintf(ff, "}\n\n")

	return NewBlob(bottom.Dim[0], numOutput, height, width)
}

func printHalfArray(f io.Writer, name string, vals []float32) {
	fmt.Fprintf(f, "constant half %s[] = {\n", name)
	for i, v := range vals {
		if i%8 == 0 {
			fmt.Fprintf(f, "    ")
		}
		fmt.Fprintf(f, "%f,", v)
		if i%8 == 7 || i == len(vals)-1 {
			fmt.Fprintln(f)
		} else {
			fmt.Fprint(f, " ")
		}
	}
	fmt.Fprintf(f, "};\n\n")
}

func printSwiftFloatArrayContents(f io.Writer, vals []float32) {
	for i, v := range vals {
		if i%8 == 0 {
			fmt.Fprintf(f, "        ")
		}
		fmt.Fprintf(f, "%f,", v)
		if i%8 == 7 || i == len(vals)-1 {
			fmt.Fprintln(f)
		} else {
			fmt.Fprint(f, " ")
		}
	}
	if len(vals)%8 != 0 {
		fmt.Fprintf(f, "\n")
	}
}
